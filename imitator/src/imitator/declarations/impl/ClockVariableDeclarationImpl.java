/**
 */
package imitator.declarations.impl;

import imitator.declarations.ClockVariableDeclaration;
import imitator.declarations.DeclarationsPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Clock Variable Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ClockVariableDeclarationImpl extends VariableDeclarationImpl implements ClockVariableDeclaration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClockVariableDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeclarationsPackage.Literals.CLOCK_VARIABLE_DECLARATION;
	}

} //ClockVariableDeclarationImpl
