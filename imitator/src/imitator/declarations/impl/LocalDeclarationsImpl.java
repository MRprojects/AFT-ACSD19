/**
 */
package imitator.declarations.impl;

import imitator.declarations.DeclarationsPackage;
import imitator.declarations.LocalDeclarations;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Local Declarations</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LocalDeclarationsImpl extends DeclarationsImpl implements LocalDeclarations {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocalDeclarationsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeclarationsPackage.Literals.LOCAL_DECLARATIONS;
	}

} //LocalDeclarationsImpl
