/**
 */
package imitator.declarations.global.impl;

import imitator.declarations.global.DefaultChannelPriority;
import imitator.declarations.global.GlobalPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Default Channel Priority</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DefaultChannelPriorityImpl extends ChannelPriorityItemImpl implements DefaultChannelPriority {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefaultChannelPriorityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GlobalPackage.Literals.DEFAULT_CHANNEL_PRIORITY;
	}

} //DefaultChannelPriorityImpl
