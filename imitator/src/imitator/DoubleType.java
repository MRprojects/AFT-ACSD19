/**
 */
package imitator;

import imitator.types.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Double Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see imitator.ImitatorPackage#getDoubleType()
 * @model
 * @generated
 */
public interface DoubleType extends Type {
} // DoubleType
