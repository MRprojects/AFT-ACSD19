/**
 */
package imitator.types.impl;

import imitator.types.TypeSpecification;
import imitator.types.TypesPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class TypeSpecificationImpl extends TypeDefinitionImpl implements TypeSpecification {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.TYPE_SPECIFICATION;
	}

} //TypeSpecificationImpl
