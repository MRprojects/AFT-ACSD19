/**
 */
package imitator.templates.impl;

import imitator.declarations.impl.VariableContainerImpl;

import imitator.templates.Selection;
import imitator.templates.TemplatesPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Selection</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SelectionImpl extends VariableContainerImpl implements Selection {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SelectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TemplatesPackage.Literals.SELECTION;
	}

} //SelectionImpl
