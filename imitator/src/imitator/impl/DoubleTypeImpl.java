/**
 */
package imitator.impl;

import imitator.DoubleType;
import imitator.ImitatorPackage;

import imitator.types.impl.TypeImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Double Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DoubleTypeImpl extends TypeImpl implements DoubleType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DoubleTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ImitatorPackage.Literals.DOUBLE_TYPE;
	}

} //DoubleTypeImpl
