/**
 */
package imitator.impl;

import imitator.DoubleType;
import imitator.ImitatorPackage;
import imitator.NSTA;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>NSTA</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link imitator.impl.NSTAImpl#getDdouble <em>Ddouble</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NSTAImpl extends NTAImpl implements NSTA {
	/**
	 * The cached value of the '{@link #getDdouble() <em>Ddouble</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDdouble()
	 * @generated
	 * @ordered
	 */
	protected DoubleType ddouble;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NSTAImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ImitatorPackage.Literals.NSTA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DoubleType getDdouble() {
		return ddouble;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDdouble(DoubleType newDdouble, NotificationChain msgs) {
		DoubleType oldDdouble = ddouble;
		ddouble = newDdouble;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ImitatorPackage.NSTA__DDOUBLE, oldDdouble, newDdouble);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDdouble(DoubleType newDdouble) {
		if (newDdouble != ddouble) {
			NotificationChain msgs = null;
			if (ddouble != null)
				msgs = ((InternalEObject)ddouble).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ImitatorPackage.NSTA__DDOUBLE, null, msgs);
			if (newDdouble != null)
				msgs = ((InternalEObject)newDdouble).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ImitatorPackage.NSTA__DDOUBLE, null, msgs);
			msgs = basicSetDdouble(newDdouble, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImitatorPackage.NSTA__DDOUBLE, newDdouble, newDdouble));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ImitatorPackage.NSTA__DDOUBLE:
				return basicSetDdouble(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ImitatorPackage.NSTA__DDOUBLE:
				return getDdouble();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ImitatorPackage.NSTA__DDOUBLE:
				setDdouble((DoubleType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ImitatorPackage.NSTA__DDOUBLE:
				setDdouble((DoubleType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ImitatorPackage.NSTA__DDOUBLE:
				return ddouble != null;
		}
		return super.eIsSet(featureID);
	}

} //NSTAImpl
