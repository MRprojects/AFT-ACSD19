/**
 */
package imitator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NSTA</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link imitator.NSTA#getDdouble <em>Ddouble</em>}</li>
 * </ul>
 *
 * @see imitator.ImitatorPackage#getNSTA()
 * @model
 * @generated
 */
public interface NSTA extends NTA {
	/**
	 * Returns the value of the '<em><b>Ddouble</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ddouble</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ddouble</em>' containment reference.
	 * @see #setDdouble(DoubleType)
	 * @see imitator.ImitatorPackage#getNSTA_Ddouble()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DoubleType getDdouble();

	/**
	 * Sets the value of the '{@link imitator.NSTA#getDdouble <em>Ddouble</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ddouble</em>' containment reference.
	 * @see #getDdouble()
	 * @generated
	 */
	void setDdouble(DoubleType value);

} // NSTA
