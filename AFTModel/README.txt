This repository contains metamodels for attack trees to be converted
into Uppaal for analysis, which is being extended to include elements
from fault trees, and to allow interpretation of the analysis results in
terms of the original trees.

========================= UPPAAL-SMC metamodel =========================

AFTModel requires the Uppaal-SMC metamodel, which is currently available
from various locations. A submodule is included to pull it from the
UTwente's VCS.

To download the Uppaal-SMC metamodel (for CLI git users, your GUI
mileage may vary):

- run 'git submodule init'
- run 'git submodule update'

This should automatically download the correct commit into the
'UppaalEMF directory'.

========================= Eclipse instructions =========================

To import the project into Eclipse, one needs various dependencies (in
particular the Eclipse Modeling Framework and the Epsilon transformation
languages). For an indication of which packages are needed, the file
`Eclipse installed software.pdf' shows the installed packages on one
installation that is able to execute all the features of ATTop (although
not all listed packages are actually needed).

Next, we need to import several projects for the Uppaal metamodel and
our main project.

- In Eclipse use "File -> Import -> (General) Existing Projects into
  Workspace". Select the 'UppaalEMF' directory as the root directory and
  make sure the option "Search for nested projects" is enabled. Tick
  (at least) the following projects:

  * nl.utwente.ewi.fmt.uppaalsmc
  * org.muml.uppaal
  * org.muml.uppaal.requirements
  * org.muml.uppaal.serialization

  Click "Finish" to import these projects.

- Again use "File -> Import -> (General) Existing Projects into
  Workspace". This time, select the 'Model' directory and import the
  'AFTModel' project.

- This should have imported all dependencies. To check, run the 'ATTGUI'
  class in "nl.utwente.ewi.fmt.UATMM.standalone".
