\documentclass[handout]{beamer}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{rotating}
\usepackage{multirow}
\usetikzlibrary{shapes.gates.logic.US}

\usetheme{ut}
\setbeamertemplate{footline}[frame number]
\usepackage{unicode-math}

\begin{document}
\title[Test]{Efficient Domain-Specific Tool Development for UPPAAL via
Model-Driven Engineering}
\author{Enno Ruijters}
\date{26 September 2017}
\institute[UT-EWI]{University of Twente}
\footlinetext{Test}

\maketitle

\newcommand{\outlineslide}{
	\frame{
	\hspace{5cm}\tableofcontents
}}
\newcommand{\outlineslideX}[1]{
	\frame{
	\begin{columns}[T]
		\column{0.48\textwidth}
		\tableofcontents[currentsection,currentsubsection]
		\column{0.48\textwidth}
		#1
	\end{columns}
}}

\section{Introduction}

\begin{frame}
\frametitle{Our contribution in a nutshell}
\begin{itemize}
	\item UPPAAL is a popular back-end model-checker.
	\item Front-end tools and translations are often developed
		ad-hoc.
		\pause
		\begin{itemize}
			\item Difficult to debug, reuse, extend, etc.
		\end{itemize}
		\pause
	\item Model-driven engineering provides more structure for
		translations.
		\pause
		\begin{itemize}
			\item Metamodels for different domains and
				UPPAAL.
		\pause
			\item Specialized languages for transformations.
		\end{itemize}
		\pause
	\item Application of MDE improves development of front-end
		tools:
		\pause
		\begin{itemize}
			\item Better interoperability
		\pause
			\item Faster development
		\pause
			\item Easier to maintain
		\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Bridging tools}
	\begin{itemize}
		\item Domain-specific tools often use generic analysis
			tools as back-ends.
		\item Typical process:
			\begin{itemize}
				\item Input domain-specific language
					(DSL)
				\item Translate to general-purpose model
					(e.g. UPPAAL timed automata)
				\item Run general-purpose analysis.
				\item Translate result back to domain.
			\end{itemize}
		\item Many tools perform this translation ad-hoc
			\begin{itemize}
				\item General language (Java, etc.)
				\item Little/no documented structure of
					either language.
				\item Closely tied to specific
					versions/features.
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Model-Driven Development}
	\begin{itemize}
		\item Have models as first-class citizens.
		\item Metamodels provide syntax and documentation of
			models.
		\item Model transformations in purpose-specific
			languages (Epsilon, etc.)
		\item Validation/constraints help in debugging and
			documentation.
	\end{itemize}
	\vfill
	\includegraphics[width=\linewidth]{img/modelTransformationConcept}
\end{frame}

\begin{frame}
	\frametitle{Metamodels}
	\begin{minipage}{0.5\textwidth}
		\begin{itemize}
			\item Describe syntax of a class of models.
			\item Object-oriented format (classes, attributes, etc.)
			\item Shown in UML-like syntax.
		\end{itemize}
	\end{minipage}\hfill\begin{minipage}{0.5\textwidth}
		\includegraphics[width=\linewidth]{img/uppaalMetamodel}
	\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{Model transformations}
	\begin{itemize}
		\item Transform one (metamodel-described) model to
			another.
		\item Transformation languages have specific constructs
			for model concepts.
	\end{itemize}
	\vfill
	\includegraphics[width=\linewidth]{img/modelTransformationConcept}
\end{frame}

{
\def\metamodel#1{{\color{cyan}#1}}
\def\keyword#1{{\color{magenta}#1}}
\def\tabindent{\phantom{AAAAA}\ifnum\indentlevel>1{\advance\indentlevel by -1\relax\tabindent{}}\fi}
\def\line#1#2{\newcount\indentlevel \indentlevel=#1\texttt{\tabindent{}#2}\\}
\begin{frame}
	\frametitle{Model transformation: \color{red}{Example}}

	\framebox{\begin{minipage}{\linewidth}
		\texttt{\keyword{rule} Base}\\
		\line1{\keyword{transform} at : \metamodel{AFT!AttackTree} \keyword{to} out : \metamodel{Uppaal!NTA} \{}
		\line1{out.systemDecl = \keyword{new} \metamodel{Uppaal!SystemDeclarations}();}
		\line1{out.systemDecl.system = \keyword{new} \metamodel{Uppaal!System}();}
		\line1{\keyword{for} (node : \metamodel{AFT!Node} \keyword{in} at.Nodes) \{}
		\line2{\keyword{var} converted = node.equivalent();}
		\line2{out.template.add(converted.get(0));}
		\line1\}
		\line1{...}
		\texttt\}\\
		\texttt{\keyword{rule} andGate \keyword{transform} node : \metamodel{AFT!Node}
		\keyword{to} ret : \metamodel{List} \{}\\
		\line1{\keyword{guard} : node.nodeType.isKindOf(\metamodel{AFT!AND})}
		\texttt{...}
	\end{minipage}
	}
\end{frame}
}

\begin{frame}
	\frametitle{UPPAAL metamodels: \color{red}{Timed automata}}
	\begin{itemize}
		\item System under analysis.
		\item Input to UPPAAL.
		\item Includes automatic transformation to XML files.
	\end{itemize}
	\begin{center}
		\includegraphics[width=0.5\linewidth]{img/uppaalMetamodel}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{UPPAAL metamodels: \color{red}{Query}}
	\begin{itemize}
		\item TCTL-like language
		\item Includes transformation to textual format.
		\item Linked to NTA for queries on specific models.
	\end{itemize}
	\begin{center}
		\includegraphics[width=0.5\linewidth]{img/requirements}
	\end{center}
\end{frame}

\begin{frame}
	\begin{itemize}
		\item Counterexamples/witnesses produced by UPPAAL.
		\item Including parser for textual output.
		\item Links back to NTA for easier interpretation.
	\end{itemize}
	\begin{center}
		\includegraphics[width=\linewidth]{img/traceMetamodel}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Examples}
	\begin{itemize}
		\item MechatronicUML project (U. of Paderborn).
		\item Synchronous dataflow graphs
		\item Attack trees
		\item Fault trees (UPPAAL-SMC)
		\item Java bytecode (timing)
	\end{itemize}
\end{frame}

\title[Test]{Part 2: Importance sampling for dynamic fault trees}
\author{Enno Ruijters}
\date{26 September 2017}
\institute[UT-EWI]{University of Twente}
\footlinetext{Test}

\maketitle

\begin{frame}
\frametitle{Our contribution in a nutshell}
\begin{itemize}
        \item Many frameworks can provide quantitative dependability
                analysis.
                \pause
                \begin{itemize}
                        \item We use dynamic fault trees.
                \pause
                        \item Compute system \textbf{availability},
                                reliability, MTTF, etc.
                \end{itemize}
                \pause
        \item Complex systems are computationally difficult to analyze:
                \pause
                \begin{itemize}
                        \item Complex $\rightarrow$ analytic approaches
                                are memory-intensive.
                \pause
                        \item Rare failures $\rightarrow$ Monte Carlo
                                simulation requires many samples.
                \end{itemize}
                \pause
        \item Our solution: rare event simulation (through importance
                sampling)
		\pause
                \begin{itemize}
                        \item Make rare events more likely.
                \pause
                        \item Compensate the final result.
                \pause
                        \item Automatically.
                \end{itemize}
                \pause
        \item \textbf{Rare event simulation + dynamic fault trees $\rightarrow$
                Faster/more accurate fault tree simulation.}
\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Comparison of RES techniques}
\centering
\begin{tabular}{p{0.45\linewidth}|p{0.51\linewidth}}
        \centering\textbf{Splitting}
        &
        \hfill\textbf{Sampling}\hfill\;
        \\
        \hline
        Requires formalization of distance
        &
        Requires specification of `rare' transitions
        \pause\\\hline
        Changes simulation engine
        &
        Changes system under simulation
        \pause\\\hline
        Good for rare events of many steps
        &
        Good for rare event of few steps
        \pause\\\hline
        Limit case: fewer runs needed
        &
        Limit case: only one run needed
	\end{tabular}

\pause
\vspace{1cm}

We use importance sampling as our system reaches the rare event after
only a few, low-probability transitions. Such models provide few points
to split the samples.
\end{frame}

\begin{frame}
	\frametitle{Path-ZVA algorithm}
        \begin{itemize}
                \item Importance sampling algorithm for Markovian
                        models.
                        \pause
                \item Divides states into three categories:
                        \begin{itemize}
                                \item `Perfect' states reached
                                        frequently.
                                \item `Bad' states reached rarely.
                                \item `Connecting' states inbetween.
                        \end{itemize}
                        \pause
                \item Estimates:
			\begin{itemize}
					\item Probability of reaching `bad' states
                                before returning to `perfect' states.
                                \item Fraction of time spend in `bad'
                                        states.
                        \end{itemize}
                        \pause
                \item Transition rates parameterized as $r \cdot
                        \epsilon^n$ with $0 < \epsilon << 1$ to indicate
                        `rareness'.
        \end{itemize}
\end{frame}

\tikzset{state/.style={circle, draw, fill=black!20}}
\begin{frame}
	\frametitle{Applying Path-ZVA to DFTs}
\begin{itemize}
        \item Basic idea: Compute state space on-the-fly.
        \item Path-ZVA stores the subset of states in dominant paths.
                \pause
        \item All other states only generated as reached, and not stored.
\end{itemize}
\pause
\begin{center}
\begin{tikzpicture}
        \node[state, fill=green] (s0) {$s_0$};
        \node[state, right of=s0, xshift=1cm] (s1) {$s_1$};
        \node[state, right of=s1, xshift=1cm] (s2) {$s_2$};
        \node[state, right of=s2, xshift=1cm, fill=red] (s3) {$s_3$};
        \node[state, left of=s0, xshift=-1cm] (s4) {$s_4$};
        \draw[->] (s0) edge[bend left] node[above] {$\lambda = 3\epsilon^2$} (s1);
        \draw[->] (s1) edge node[above] {$\lambda = 2\epsilon^1$} (s2);
        \draw[->] (s2) edge node[above] {$\lambda = 2\epsilon^1$} (s3);
        \draw[->] (s1) edge[bend left] node[below] {$\lambda = \epsilon^0$} (s0);
        \draw[->] (s2) edge[in=90, out=90] node[above] {$\lambda = \epsilon^0$} (s0);
	\draw[->] (s3) edge[in=290, out=250] node[above] {$\lambda = \epsilon^0$} (s0);
        \draw[->] (s0) edge[bend right] node[above] {$\lambda = 2\epsilon^1$} (s4);
        \draw[->] (s4) edge[bend right] node[below] {$\lambda = \epsilon^0$} (s0);
        \draw[->,dashed] (s4) -- ++(-1cm,0);
\end{tikzpicture}
\end{center}
\end{frame}

\frame{\frametitle{Results: Accuracy}
\newif\ifrc
\rcfalse
\newif\ifftpp
\ftpptrue
\newif\ifhecs
\hecstrue
Exact result for DFTCalc, 95\% confidence for others:
\resizebox{\textwidth}{!}{\input{resulttable}}
}

\frame{\frametitle{Overall results: State space}
\includegraphics[width=\textwidth]{img/states_processed}
\vspace{5mm}
\begin{itemize}
        \item FTRES always below DFTCalc maximal state space size.
        \item FTRES computes results where DFTCalc does not.
\end{itemize}
}

\frame{\frametitle{Overall results: Speed}
\includegraphics[width=\textwidth]{img/times_processed}
\vspace{3mm}
\begin{itemize}
        \item FTRes and MC spend a constant 5 mins. simulating.
        \item Simulation time mostly dominates state-space exploration.
        \item Almost all DFTCalc experiments for HECS ran out of memory.
\end{itemize}
}

\begin{frame}
	\centering
	\Huge{Thank you for your attention.}\\
	\vspace{2cm}
	\Huge{Questions?}
\end{frame}
\end{document}
