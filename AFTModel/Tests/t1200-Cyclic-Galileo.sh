# Copyright 2009 Enno Ruijters
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 2 of the GNU General
# Public License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#!/bin/sh

# Attempt to convert a cyclic DFT model into an Uppaal model and confirm
# that the program generates an error instead of a model.

. ./test-lib.sh

if [ "$1" != "do_test" ]; then
	test_begin "t1200-Cyclic-Galileo" "$0"
	exit $?
fi

load_file t1200-cycle.dft

test_and_stop_on_error "Fail to convert from Galileo to ATCalc" \
                       "! $TESTPROG -i Galileo t1200-cycle.dft -o Uppaal test.xml >/dev/null 2>/dev/null"

test_and_continue "Confirm that no model output exists" \
                  "! [ -e test.xml ]"

test_end
