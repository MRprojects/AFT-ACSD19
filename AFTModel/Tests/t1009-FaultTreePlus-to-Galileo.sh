# Copyright 2009 Enno Ruijters
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 2 of the GNU General
# Public License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#!/bin/sh

# Convert an ADTool tree into an ATE file and check that the resulting
# file can be correctly analysed by ATE.

. ./test-lib.sh

if [ "$1" != "do_test" ]; then
	test_begin ${0%.sh} "$0"
	exit $?
fi

load_file FTP.rwbx
load_file FTP.dft

test_and_stop_on_error "Execute conversion from FaultTree+ to ATCalc" \
                       "$TESTPROG -i FaultTreePlus FTP.rwbx -o ATCalc out.dft > /dev/null"

test_and_continue "Resulting output is identical to stored version" \
                  "cmp out.dft FTP.dft"

test_end
