# Copyright 2009 Enno Ruijters
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 2 of the GNU General
# Public License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#!/bin/sh

# Convert an ADTool tree into a binary tree in ADTool format and check
# that the resulting tree is correct.

. ./test-lib.sh

if [ "$1" != "do_test" ]; then
	test_begin "t1002-ADTool-to-ADTBin" "$0"
	exit $?
fi

load_file ADT.xml
load_file ADTBin.xml

test_and_stop_on_error "Execute conversion from ADTool to binary ADTool" \
                       "$TESTPROG --keep-temporary-files -i ADTool ADT.xml -o ADTBin output";

test_and_continue "Resulting file is correct" \
                  "cmp -s output ADTBin.xml"

test_end
