# Copyright 2009 Enno Ruijters
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 2 of the GNU General
# Public License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#!/bin/sh

# Convert a static fault tree with maintenance to Uppaal, and compare
# that the correct result (0.37 +- 0.01) is validated with 99%
# confidence by Uppaal. Note that failures of this test may occur due to
# coincidence, but not frequently.

. ./test-lib.sh

if [ "$1" != "do_test" ]; then
	test_begin ${0%.sh} "$0"
	exit $?
fi

load_file mainttest.dft
load_file mainttest.maint
load_file t1011-options.txt
load_file t1011-queries.txt

test_and_stop_on_error "Execute conversion from Galileo+Maintenance to Uppaal result" \
                       "$TESTPROG -i Galileo mainttest.dft -i AFTM mainttest.maint -i UppaalOptions t1011-options.txt -i UppaalTextQuery t1011-queries.txt -o UppaalTextResult output"

test_and_continue "Result is bounded by computed 99% confidence interval" \
                  "grep 'is satisfied' output | wc -l | grep -q '^2$'"

test_end
