# Copyright 2009 Enno Ruijters
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 2 of the GNU General
# Public License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#!/bin/sh

# Convert an ADTool tree into an Uppaal XML files and check that the
# resulting tree is correct.

. ./test-lib.sh

if [ "$1" != "do_test" ]; then
	test_begin "t1006-ADTool-to-Uppaal-result" "$0"
	exit $?
fi

load_file ADT.xml
load_file t1006-query.txt
load_file t1006-options.txt

test_and_stop_on_error "Execute conversion from ADTool to Uppaal result" \
                       "$TESTPROG -i ADTool ADT.xml -i UppaalOptions t1006-options.txt -i UppaalTextQuery t1006-query.txt -o UppaalTextResult output > /dev/null"

test_and_continue "Result is correct" \
                  "grep -q 'Formula is satisfied' output"

test_end
