# Copyright 2009 Enno Ruijters
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 2 of the GNU General
# Public License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#!/bin/sh

# Convert an ADTool tree into an Uppaal XML files and check that the
# resulting tree is correct.

. ./test-lib.sh

if [ "$1" != "do_test" ]; then
	test_begin "t1007-Galileo-with-costs-to-Uppaal-result" "$0"
	exit $?
fi

load_file AND+cost.dft
load_file t1007-query.txt

test_and_stop_on_error "Execute conversion from Galileo to Uppaal result" \
                       "$TESTPROG -i Galileo AND+cost.dft -i UppaalTextQuery t1007-query.txt -o UppaalTextResult output > /dev/null"

test_and_continue "Reachable result is correct" \
                  "grep -q 'Formula is satisfied' output"

test_and_continue "Unreachable result is correct" \
                  "grep -q 'Formula is NOT satisfied' output"

test_end
