# Copyright 2009 Enno Ruijters
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 2 of the GNU General
# Public License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#!/bin/sh

usage() {
	echo "Usage: run-test.sh <testnr> [--keep] [--keep-failed]"
}

if [ -z "$1" ]; then
	usage;
	exit 0;
fi
TESTNR="$1";
shift;

while true; do
	if [ -z "$1" ]; then
		break;
	fi
	case "$1" in
		-h|--help|-\?) usage; exit 0;;
		--keep) export KEEP_TRASH=1; shift;;
		--keep-failed) export KEEP_FAILED=1; shift;;
		*) echo "Unknown parameter $1"; exit 0;;
	esac
done

for i in t$TESTNR-*.sh; do
	sh $i;
done
