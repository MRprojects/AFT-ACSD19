# Copyright 2009 Enno Ruijters
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 2 of the GNU General
# Public License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#!/bin/sh

# Convert a simple ATA file to UAT format and check that the
# resulting files are correct.

. ./test-lib.sh

if [ "$1" != "do_test" ]; then
	test_begin "t1004-ATA-input" "$0"
	exit $?
fi

load_file ATA_SMALL.xml
load_file t1004-ATA.struct
load_file t1004-ATA.values

test_and_stop_on_error "Execute conversion from ATA to UAT" \
                       "$TESTPROG -i ATA ATA_SMALL.xml -o UATS struct -o UATV values > /dev/null"

test_and_continue "Structure file is correct" \
                  "cmp -s struct t1004-ATA.struct"

test_and_continue "Values file is correct" \
                  "cmp -s values t1004-ATA.values"

test_end
