# Copyright 2009 Enno Ruijters
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 2 of the GNU General
# Public License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#!/bin/sh

# Convert an ADTool tree into an ATE file and check that the resulting
# file can be correctly analysed by ATE.

. ./test-lib.sh

if [ "$1" != "do_test" ]; then
	test_begin ${0%.sh} "$0"
	exit $?
fi

load_file EnterBuilding.xml
load_file ATE.jar
load_file t1008-output.txt

test_and_stop_on_error "Execute conversion from ADTool to ATE" \
                       "$TESTPROG -i ADTool EnterBuilding.xml -o ATE ATE.xml > /dev/null"

test_and_continue "ATE can analyze resulting model" \
                  "LC_ALL=C java -jar ATE.jar ATE.xml pareto > output"

test_and_continue "Resulting output is correct" \
                  "cmp output.txt t1008-output.txt"

test_end
