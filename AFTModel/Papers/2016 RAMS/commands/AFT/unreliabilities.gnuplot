set terminal pdf
set ylabel 'Unreliability'
set xlabel 'Time'
set yrange [0:0.45]
plot 'parsed.txt' using 1:($2*16) with lines linewidth 1 linecolor rgbcolor '#00FF00' title 'Cost 0', \
'parsed.txt' using 1:($3*16) with lines linewidth 1 linecolor rgbcolor '#00FF00' notitle, \
'parsed.txt' using 1:($4*8) with lines linewidth 1 linecolor rgbcolor '#00FFFF' title 'Cost 10', \
'parsed.txt' using 1:($5*8) with lines linewidth 1 linecolor rgbcolor '#00FFFF' notitle,\
'parsed.txt' using 1:($6*16) with lines linewidth 1 linecolor rgbcolor '#FF00FF' title 'Cost 20', \
'parsed.txt' using 1:($7*16) with lines linewidth 1 linecolor rgbcolor '#FF00FF' notitle,\
'parsed.txt' using 1:($8*8) with lines linewidth 1 linecolor rgbcolor '#0000FF' title 'Cost 100', \
'parsed.txt' using 1:($9*8) with lines linewidth 1 linecolor rgbcolor '#0000FF' notitle,\
'parsed.txt' using 1:($10*16) with lines linewidth 1 linecolor rgbcolor '#FF0000' title 'Cost 200', \
'parsed.txt' using 1:($11*16) with lines linewidth 1 linecolor rgbcolor '#FF0000' notitle
