merge_lines () {
	for TIME in $1; do
		OUT="$TIME";
		i=0;
		while [ "$i" -lt "5" ]; do
			read LINE;
			OUT="$OUT $LINE";
			i=$(( $i + 1 ));
		done
		echo "$OUT";
	done
}

grep 'Pr' "$2" | grep -o '\[.*\]$' | sed -e 's/,\|\]\|\[/ /g' | merge_lines "$1"
