#!/bin/sh

rm allqueries.txt
VALUES="0 1 2 3 4 5 6 7 8 9 10 15 20 24 25 30 40 50";
for i in $VALUES; do
	sed -e "s/T/$i/g" < queries.txt >> allqueries.txt;
done
/home/ruijtersejj/java8/bin/java -jar ../UAT.jar -i Galileo 'Case Study AFT.dft' -i \
UppaalOptions options.txt -i UppaalTextQuery allqueries.txt -o \
UppaalTextResult result.txt

sh parse.sh "$VALUES" result.txt > parsed.txt
#gnuplot < unreliabilities.gnuplot > unreliabilities.pdf
