merge_lines () {
	while read LINE; do
		read OTHER;
		echo "$LINE $OTHER";
	done
}

grep 'formula\|Pr' "$1" | grep -o '\(\[.*\]$\)\|[[:digit:]]\+$' | sed -e 's/,\|\]\|\[/ /g' | merge_lines
