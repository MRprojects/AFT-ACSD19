set terminal pdf
set ylabel 'Unreliability'
set xlabel 'Time'
set key at 2,0.2
plot 'parsed.txt' using ($1/100):2 with lines linewidth 1 linecolor rgbcolor '#00FF00' title 'Lower bound with 99% confidence', \
'parsed.txt' using ($1/100):3 with lines linewidth 1 linecolor rgbcolor '#FF0000' title 'Upper bound with 99% confidence', \
'FTplus.txt' using 1:2 with lines linewidth 1 linecolor rgbcolor '#0000FF' title 'Isograph FaultTree+'
