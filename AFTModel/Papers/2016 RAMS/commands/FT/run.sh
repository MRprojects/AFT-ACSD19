#!/bin/sh

java -jar ../UAT.jar -i FaultTreePlus 'Case Study Cooling.rwbx' -i \
UppaalOptions options.txt -i UppaalTextQuery queries.txt -o \
UppaalTextResult result.txt

sh parse.sh result.txt > parsed.txt
gnuplot < unreliability.gnuplot > unreliability.pdf
