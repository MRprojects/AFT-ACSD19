
(************************************************************
 *                      IMITATOR MODEL                      
 *
 * Encoding of an attack-tree
 *
 * Description     : TODO
 * Correctness     : Success should be reached
 * Source          : TOTO
 * Author          : TODO
 * Modeling        : TODO
 * Input by        : TODO
 * License         : Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
 *
 * Generated       : Thu Jan 10 21:33:04 CET 2019
 * Last modified   : yyyy/mm/dd
 *
 * IMITATOR version: 2.10
 ************************************************************)


var

(* Clocks *)

x_FW, 
x_FLAP, 
x_SMA, 
x_BWK, 
x_ESV, 
x_RMS, 
x_GC, 
abs_time,
		: clock;
	
	
(* Timing parameters *)
	
maxtime_FW=300.0, 
mintime_FW=300.0, 
cost_FW=10.0, 
maxtime_FLAP=60.0, 
cost_FLAP=10.0, 
mintime_FLAP=60.0, 
cost_SMA=50.0, 
maxtime_SMA=30.0, 
mintime_SMA=30.0, 
cost_BWK=100.0, 
maxtime_BWK=120.0, 
mintime_BWK=120.0, 
maxtime_ESV=60.0, 
mintime_ESV=60.0, 
cost_ESV=10.0, 
maxtime_RMS=30.0, 
mintime_RMS=30.0, 
cost_RMS=100.0, 
cost_GC=100.0, 
maxtime_GC=600.0, 
mintime_GC=600.0, 
total_cost,
total_time,
		: parameter;


(* Fake discret *)

	current_cost_root,
current_cost_AW,
current_cost_AHN,
current_cost_AL,
current_cost_CID,
current_cost_GATPN,
	: clock;



(************************************************************)
  automaton rootTA
(************************************************************)
synclabs: startCID, failRoot, successCID, successRoot;

urgent loc l1: invariant True stop {current_cost_root, current_cost_AW, current_cost_AHN, current_cost_AL, current_cost_CID, current_cost_GATPN,}
	when True sync startCID goto l2;

loc l2: invariant True stop {current_cost_root, current_cost_AW, current_cost_AHN, current_cost_AL, current_cost_CID, current_cost_GATPN,}
	when True sync failRoot goto fail;
	when True sync successCID goto almost_success;

(* Note: need to test the guard AFTER the synchronized action, otherwise the cost is not yet updated :( *)
urgent loc almost_success: invariant True
	when current_cost_root = total_cost & total_time = abs_time sync successRoot goto success;

loc fail: invariant True stop {current_cost_root, current_cost_AW, current_cost_AHN, current_cost_AL, current_cost_CID, current_cost_GATPN,}

urgent loc success: invariant True stop {current_cost_root, current_cost_AW, current_cost_AHN, current_cost_AL, current_cost_CID, current_cost_GATPN,}

end (* pta *)



(************************************************************)
  automaton ANDgateAW
(************************************************************)
synclabs: startAW, successAW, failAW, startFW, successFW, failFW, startBWK, successBWK, failBWK, ;


loc l0: invariant True
	when True sync startAW goto l1;

urgent loc l1: invariant True
	when True sync startFW goto l2;

urgent loc l2: invariant True
	when True sync startBWK goto l3;

loc l3: invariant True
	when True sync successFW goto l4;
	when True sync successBWK goto l6;
    when True sync failFW goto failing;
    when True sync failBWK goto failing;
	
loc l4: invariant True
	when True sync successBWK goto l5;
    when True sync failBWK goto failing;

loc l6: invariant True
	when True sync successFW goto l5;
    when True sync failFW goto failing;

urgent loc l5: invariant True
	when True sync successAW do {current_cost_GATPN := current_cost_GATPN + current_cost_AW} goto success;
loc success: invariant True

urgent loc failing: invariant True
    when True sync failAW goto fail;

loc fail: invariant True

end (* pta *)


(************************************************************)
  automaton ANDgateAHN
(************************************************************)
synclabs: startAHN, successAHN, failAHN, startGATPN, successGATPN, failGATPN, startGC, successGC, failGC, ;


loc l0: invariant True
	when True sync startAHN goto l1;

urgent loc l1: invariant True
	when True sync startGATPN goto l2;

urgent loc l2: invariant True
	when True sync startGC goto l3;

loc l3: invariant True
	when True sync successGATPN goto l4;
	when True sync successGC goto l6;
    when True sync failGATPN goto failing;
    when True sync failGC goto failing;
	
loc l4: invariant True
	when True sync successGC goto l5;
    when True sync failGC goto failing;

loc l6: invariant True
	when True sync successGATPN goto l5;
    when True sync failGATPN goto failing;

urgent loc l5: invariant True
	when True sync successAHN do {current_cost_CID := current_cost_CID + current_cost_AHN} goto success;
loc success: invariant True

urgent loc failing: invariant True
    when True sync failAHN goto fail;

loc fail: invariant True

end (* pta *)


(************************************************************)
  automaton ANDgateAL
(************************************************************)
synclabs: startAL, successAL, failAL, startFLAP, successFLAP, failFLAP, startSMA, successSMA, failSMA, ;


loc l0: invariant True
	when True sync startAL goto l1;

urgent loc l1: invariant True
	when True sync startFLAP goto l2;

urgent loc l2: invariant True
	when True sync startSMA goto l3;

loc l3: invariant True
	when True sync successFLAP goto l4;
	when True sync successSMA goto l6;
    when True sync failFLAP goto failing;
    when True sync failSMA goto failing;
	
loc l4: invariant True
	when True sync successSMA goto l5;
    when True sync failSMA goto failing;

loc l6: invariant True
	when True sync successFLAP goto l5;
    when True sync failFLAP goto failing;

urgent loc l5: invariant True
	when True sync successAL do {current_cost_GATPN := current_cost_GATPN + current_cost_AL} goto success;
loc success: invariant True

urgent loc failing: invariant True
    when True sync failAL goto fail;

loc fail: invariant True

end (* pta *)


(************************************************************)
  automaton SparegateCID
(************************************************************)
synclabs: startCID, successCID, failCID, startAHN, successAHN, failAHN, startESV, successESV, failESV, startRMS, successRMS, failRMS, ;

loc l0: invariant True
	when True sync startCID goto l1;

urgent loc l1: invariant True
	when True sync startAHN goto l2;
	
loc l2: invariant abs_time >= 0
    when True sync failAHN goto l3;
    
urgent loc l3: invariant True
	when True sync startESV goto l4;
	
loc l4: invariant abs_time >= 0
    when True sync failESV goto l5;
    
urgent loc l5: invariant True
	when True sync startRMS goto l6;
	
loc l6: invariant abs_time >= 0
    when True sync failRMS goto l7;
    
urgent loc l7: invariant True
	when True sync failCID do {current_cost_root := current_cost_root + current_cost_CID} goto fail;

loc fail: invariant True

end (* pta *)


(************************************************************)
  automaton ORgateGATPN
(************************************************************)
synclabs: startGATPN, successGATPN, failGATPN, startAL, successAL, failAL, startAW, successAW, failAW, ;

loc l0: invariant True
	when True sync startGATPN goto l1;

urgent loc l1: invariant True
	when True sync startAL goto l2;

urgent loc l2: invariant True
	when True sync startAW goto l3;

loc l3: invariant True
	when True sync successAL goto l4;
	when True sync successAW goto l4;

urgent loc l4: invariant True
	when True sync successGATPN do {current_cost_AHN := current_cost_AHN + current_cost_GATPN} goto success;
loc success: invariant abs_time >= 0


end (* pta *)



(************************************************************)
  automaton FW
(************************************************************)
synclabs: startFW, successFW, failFW;
	
loc l1: invariant True
	when True sync startFW do {x_FW := 0} goto l2;

loc l2: invariant x_FW <= maxtime_FW
	when x_FW >= mintime_FW do {current_cost_AW := current_cost_AW + cost_FW} sync successFW goto l3;
	when x_FW >= mintime_FW sync failFW goto l4;
	
loc l3: invariant True
loc l4: invariant True

end (* pta *)


(************************************************************)
  automaton FLAP
(************************************************************)
synclabs: startFLAP, successFLAP, failFLAP;
	
loc l1: invariant True
	when True sync startFLAP do {x_FLAP := 0} goto l2;

loc l2: invariant x_FLAP <= maxtime_FLAP
	when x_FLAP >= mintime_FLAP do {current_cost_AL := current_cost_AL + cost_FLAP} sync successFLAP goto l3;
	when x_FLAP >= mintime_FLAP sync failFLAP goto l4;
	
loc l3: invariant True
loc l4: invariant True

end (* pta *)


(************************************************************)
  automaton SMA
(************************************************************)
synclabs: startSMA, successSMA, failSMA;
	
loc l1: invariant True
	when True sync startSMA do {x_SMA := 0} goto l2;

loc l2: invariant x_SMA <= maxtime_SMA
	when x_SMA >= mintime_SMA do {current_cost_AL := current_cost_AL + cost_SMA} sync successSMA goto l3;
	when x_SMA >= mintime_SMA sync failSMA goto l4;
	
loc l3: invariant True
loc l4: invariant True

end (* pta *)


(************************************************************)
  automaton BWK
(************************************************************)
synclabs: startBWK, successBWK, failBWK;
	
loc l1: invariant True
	when True sync startBWK do {x_BWK := 0} goto l2;

loc l2: invariant x_BWK <= maxtime_BWK
	when x_BWK >= mintime_BWK do {current_cost_AW := current_cost_AW + cost_BWK} sync successBWK goto l3;
	when x_BWK >= mintime_BWK sync failBWK goto l4;
	
loc l3: invariant True
loc l4: invariant True

end (* pta *)


(************************************************************)
  automaton ESV
(************************************************************)
synclabs: startESV, successESV, failESV;
	
loc l1: invariant True
	when True sync startESV do {x_ESV := 0} goto l2;

loc l2: invariant x_ESV <= maxtime_ESV
	when x_ESV >= mintime_ESV do {current_cost_CID := current_cost_CID + cost_ESV} sync successESV goto l3;
	when x_ESV >= mintime_ESV sync failESV goto l4;
	
loc l3: invariant True
loc l4: invariant True

end (* pta *)


(************************************************************)
  automaton RMS
(************************************************************)
synclabs: startRMS, successRMS, failRMS;
	
loc l1: invariant True
	when True sync startRMS do {x_RMS := 0} goto l2;

loc l2: invariant x_RMS <= maxtime_RMS
	when x_RMS >= mintime_RMS do {current_cost_CID := current_cost_CID + cost_RMS} sync successRMS goto l3;
	when x_RMS >= mintime_RMS sync failRMS goto l4;
	
loc l3: invariant True
loc l4: invariant True

end (* pta *)


(************************************************************)
  automaton GC
(************************************************************)
synclabs: startGC, successGC, failGC;
	
loc l1: invariant True
	when True sync startGC do {x_GC := 0} goto l2;

loc l2: invariant x_GC <= maxtime_GC
	when x_GC >= mintime_GC do {current_cost_AHN := current_cost_AHN + cost_GC} sync successGC goto l3;
	when x_GC >= mintime_GC sync failGC goto l4;
	
loc l3: invariant True
loc l4: invariant True

end (* pta *)



(************************************************************)
(* Initial state *)
(************************************************************)

init :=
	(*------------------------------------------------------------*)
	(* Initial location *)
	(*------------------------------------------------------------*)
	
	& loc[rootTA] = l1
	
	& loc[ANDgateAW] = l0
	& loc[ANDgateAHN] = l0
	& loc[ANDgateAL] = l0
	& loc[SparegateCID] = l0
	& loc[ORgateGATPN] = l0

	& loc[FW] = l1
	& loc[FLAP] = l1
	& loc[SMA] = l1
	& loc[BWK] = l1
	& loc[ESV] = l1
	& loc[RMS] = l1
	& loc[GC] = l1

	(*------------------------------------------------------------*)
	(* Initial clock constraints *)
	(*------------------------------------------------------------*)

	& abs_time = 0

	& x_FW = 0 
	& x_FLAP = 0 
	& x_SMA = 0 
	& x_BWK = 0 
	& x_ESV = 0 
	& x_RMS = 0 
	& x_GC = 0 

	(*------------------------------------------------------------*)
	(* Parameter constraints *)
	(*------------------------------------------------------------*)
	& total_cost >= 0
	& total_time >= 0

	& maxtime_FW >= 0
	& mintime_FW >= 0
	& cost_FW >= 0
	& maxtime_FLAP >= 0
	& cost_FLAP >= 0
	& mintime_FLAP >= 0
	& cost_SMA >= 0
	& maxtime_SMA >= 0
	& mintime_SMA >= 0
	& cost_BWK >= 0
	& maxtime_BWK >= 0
	& mintime_BWK >= 0
	& maxtime_ESV >= 0
	& mintime_ESV >= 0
	& cost_ESV >= 0
	& maxtime_RMS >= 0
	& mintime_RMS >= 0
	& cost_RMS >= 0
	& cost_GC >= 0
	& maxtime_GC >= 0
	& mintime_GC >= 0

	(*------------------------------------------------------------*)
	(* Discrete constraints *)
	(*------------------------------------------------------------*)

    & current_cost_root = 0
    
	& current_cost_AW = 0
	& current_cost_AHN = 0
	& current_cost_AL = 0
	& current_cost_CID = 0
	& current_cost_GATPN = 0
;


(************************************************************)
(* Property specification *)
(************************************************************)

property := unreachable loc[rootTA] = success;
minimize(total_cost);


(************************************************************)
(* The end *)
(************************************************************)
end



