package nl.utwente.ewi.fmt.UATMM.transformers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import nl.utwente.ewi.fmt.UATMM.standalone.Language;

public class UppaalParetoCalculator implements ITransformer
{
	public final static UppaalParetoCalculator INSTANCE = new UppaalParetoCalculator();
	
	private static String readOptions(String filename) throws java.io.IOException
	{
		byte data[] = new byte[4096];
		FileInputStream in = new FileInputStream(filename);
		in.read(data);
		in.close();
		return new String(data);
	}

	private int readAttr(String attr, File resultFile) throws IOException
	{
		int ret = -1;
		try (FileInputStream in = new FileInputStream(resultFile)) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String line;
			while ((line = reader.readLine()) != null) {
				int idx = line.indexOf(attr + "=");
				if (idx >= 0) {
					String sub = line.substring(idx + attr.length() + 1);
					idx = sub.indexOf(" ");
					sub = sub.substring(0, idx);
					ret = Integer.valueOf(sub);
				}
			}
		}
		return ret;
	}

	@Override
	public Object execute(Map<String, Language> inputs,
			Map<Language, String> outputs) throws Exception
	{
		String model = null, options = "";
		for (Map.Entry<String, Language> e : inputs.entrySet()) {
			if (e.getValue() == Language.UPPAAL_XML)
				model = e.getKey();
			else if (e.getValue() == Language.UPPAAL_OPTIONS)
				options = readOptions(e.getKey());
		}

		assert(model != null); /* Should be enforced by transformation search */
		TreeMap<String, Language> uppaalInputs = new TreeMap<>();
		uppaalInputs.put(model, Language.UPPAAL_XML);
		
		File outputFile = new File(outputs.get(Language.UPPAAL_PARETO_RESULT));
		File optsFile = File.createTempFile("uppaalOptions", ".txt");
		optsFile.deleteOnExit();
		try (FileOutputStream str = new FileOutputStream(optsFile)) {
			str.write((options + "-t 2 -o 3").getBytes());
		}
		uppaalInputs.put(optsFile.getPath(), Language.UPPAAL_OPTIONS);
		File queryFile = File.createTempFile("uppaalQuery", ".txt");
		queryFile.deleteOnExit();
		try (FileOutputStream str = new FileOutputStream(queryFile)) {
			str.write("E<> toplevel.Completed".getBytes());
		}
		uppaalInputs.put(queryFile.getPath(), Language.UPPAAL_TEXT_QUERY);
		File tempOutFile = File.createTempFile("uppaalTempOut", ".txt");
		tempOutFile.deleteOnExit();
		Map<Language, String> uppaalOutput = Collections.singletonMap(Language.UPPAAL_TEXT_RESULT, tempOutFile.getPath());

		System.err.println("Finding shortest solution");
		UppaalExecutingTransformer.instance().execute(uppaalInputs, uppaalOutput);
		int curCost = readAttr("cost", tempOutFile);
		int curTime = readAttr("toplevel.time", tempOutFile);
		System.err.println("Cost " + curCost);
		try (FileOutputStream out = new FileOutputStream(outputFile)) {
			out.write("'Time';'Cost'\n".getBytes());
			while (curCost > 0) {
				try (FileOutputStream str = new FileOutputStream(queryFile)) {
					str.write(("E<> toplevel.Completed && cost < " + curCost).getBytes());
				}
				System.err.println("Finding solution cheaper than " + curCost);
				UppaalExecutingTransformer.instance().execute(uppaalInputs, uppaalOutput);
				int newCost = readAttr("cost", tempOutFile);
				int newTime = readAttr("time", tempOutFile);
				if (newTime > curTime || newTime == -1)
					out.write(String.format("%d;%d\n", curTime, curCost).getBytes());
				curCost = newCost;
				curTime = newTime;
			}
		}
		try (FileOutputStream outStr = new FileOutputStream(outputFile, true)) {
		}

		return null;
	}

	@Override
	public Set<Language> getSourceLanguages()
	{
		return EnumSet.of(Language.UPPAAL_XML);
	}

	@Override
	public Set<Language> getTargetLanguages()
	{
		return EnumSet.of(Language.UPPAAL_PARETO_RESULT);
	}
	
	@Override
	public Set<Language> getOptionalSourceLanguages()
	{
		return Collections.singleton(Language.UPPAAL_OPTIONS);
	}

}
