package nl.utwente.ewi.fmt.UATMM.transformers;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

import nl.utwente.ewi.fmt.UATMM.standalone.Language;
import nl.utwente.ewi.fmt.UATMM.Maintenance.MaintenancePolicy;
import nl.utwente.ewi.fmt.UATMM.Maintenance.impl.MaintenanceFactoryImpl;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.epsilon.emc.emf.InMemoryEmfModel;
import org.eclipse.epsilon.eol.IEolModule;
import org.eclipse.epsilon.eol.exceptions.models.EolModelLoadingException;
import org.eclipse.epsilon.eol.models.IModel;
import org.eclipse.epsilon.etl.EtlModule;

public class UAT2ImitatorETL extends EpsilonTransformer {
	/** The singleton instance of this transformer. */
	private static UAT2ImitatorETL INSTANCE;
	public static boolean USE_CORA = false;
	
	/** Returns the singleton instance of this transformer. */
	public static UAT2ImitatorETL instance() {
		if (INSTANCE == null) {
			INSTANCE = new UAT2ImitatorETL();
		}
		return INSTANCE;
	}

	/** Constructor for the singleton instance of this transformer. */
	private UAT2ImitatorETL() {
		super(Arrays.asList(new Language[]{Language.UATS, Language.UATV}), Arrays.asList(new Language[]{Language.IMITATORETL}));
	}
	
	@Override
	public Set<Language> getOptionalSourceLanguages()
	{
		return Collections.singleton(Language.UATM);
	}

	@Override
	public IEolModule createModule() {
		return new EtlModule();
	}

	@Override
	public HashSet<IModel> getModels(Map<String, Language> inputs, Map<Language, String> outputs, Language output)
			throws EolModelLoadingException, URISyntaxException, IOException
	{
		HashSet<IModel> models = new HashSet<IModel>();
		boolean haveStructure = false, haveMaintenance = false;
		for (Map.Entry<String, Language> e : inputs.entrySet()) {
			if (e.getValue() == Language.UATS || e.getValue() == Language.UATS_BINARY) {
				models.add(createEmfModel(Language.UATS, Role.SOURCE, e.getKey()));
				if (haveStructure) {
					throw new UnsupportedOperationException("Multiple structure inputs");
				}
				haveStructure = true;
			} else if (e.getValue() == Language.UATV) {
				models.add(createEmfModel(e.getValue(), Role.SOURCE, e.getKey()));
			} else if (e.getValue() == Language.UATM) {
				models.add(createEmfModel(e.getValue(), Role.SOURCE, e.getKey()));
				haveMaintenance = true;
			}
		}
		if (!haveMaintenance) {
			/* Construct an empty model as a blank maintenance policy */
			MaintenancePolicy emptyPolicy = MaintenanceFactoryImpl.eINSTANCE.createMaintenancePolicy();
			ResourceSet rs = new ResourceSetImpl();
			EPackage ePackage = emptyPolicy.eClass().getEPackage();
			rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new XMIResourceFactoryImpl());
			rs.getPackageRegistry().put(ePackage.getNsURI(), ePackage);
			Resource r = rs.createResource(URI.createFileURI(""));
			r.getContents().add(emptyPolicy);
			InMemoryEmfModel m = new InMemoryEmfModel(Language.UATM.getName(), r);
			models.add(m);
		}
		if (!haveStructure)
			throw new UnsupportedOperationException("No UAT structure model provided");
		if (outputs.size() > 1)
			throw new UnsupportedOperationException("More than one output requested");
		if (output != Language.IMITATORETL)
			throw new UnsupportedOperationException("UAT2Uppaal cannot produce " + output + " outputs");
		System.out.println("tryina create model "+outputs.get(Language.IMITATORETL)+" "+Role.TARGET+" "+Language.IMITATORETL);
		models.add(createEmfModel(Language.IMITATORETL, Role.TARGET, outputs.get(Language.IMITATORETL)));
		System.out.println("model created "+outputs.get(Language.IMITATORETL)+" "+Role.TARGET+" "+Language.IMITATORETL);
		return models;
	}

	@Override
	public Map<Language, String> getTransformations() {
		EnumMap<Language, String> ret = new EnumMap<Language, String>(Language.class);
		ret.put(Language.IMITATORETL, "transformations/FullModel/UATMM2imitator.etl");
		return ret;
	}
}
