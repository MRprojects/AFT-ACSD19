## Attack tree to imitator extension

Attack Tree translator written in Java using the [Eclipse Modeling Project](https://eclipse.dev/modeling/).
Extends [UTwente ATTop tool](https://github.com/utwente-fmt/attop)


### Associated paper: [Parametric analyses of attack-fault trees](https://arxiv.org/abs/1902.04336)

See the translation model [``AFTModel/Model/data/transformations/UAT2Imitator.egl``](./AFTModel/Model/data/transformations/UAT2Imitator.egl) used to generate [the files described](https://lipn.univ-paris13.fr/~ramparison/ACSD2019/) in the paper [Parametric analyses of attack-fault trees](https://arxiv.org/abs/1902.04336).


### To add the metamodel to AFTmodel

![step one](./addimitatortoaftmodel1.png)
![step two](./addimitatortoaftmodel2.png)


### To run the tranlation tool

![step one](./runattop1.png)
![step two](./runattop2.png)
